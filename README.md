## scala-sbt-vertx.g8
A `Giter8` template to quickly bootstrap a production-ready, opinionated `scala` project to build a microservice in `Vertx`

### What's in the Box??
+ `scoverage` configurations for code coverage
+ `scalastyle` configurations for code formatting 
+ `scapegoat` configurations for static code analysis
+ `sonarQube` configurations for continuous inspection
+ `vertx` boilerplate for an HTTP webserver
+ `circe` for json parsing
+ `scalatest` for unit testing
+ `scalacheck` for property-based testing
+ `logback` configurations for logging

### Prerequisites
+ Java 1.8+
+ SBT 1.0.0+
```bash
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt
```
+ Giter8
```bash
# First downlooads Conscript
curl https://raw.githubusercontent.com/foundweekends/conscript/master/setup.sh | sh

# Download Giter8 - This takes a while. Go get a coffee
~/.conscript/bin/cs foundweekends/giter8

# Update PATH in bashrc or zshrc
PATH=$PATH:~/.conscript/bin
```
+ SonarScanner 3.2.0+
+ `sonar-scala` plugin for SonarQube - [Copy JAR](https://dl.bintray.com/mwz/maven/com/github/mwz/sonar-scala_2.12/6.5.1/sonar-scala_2.12-6.5.1-assembly.jar) to `/opt/sonarqube/extensions/plugins`
+ SonarQube 6.7+ _Optional to install locally_

### Configurations
These configurations would be required when bootstrapping new project
+ `<name>`
+ `<version>`
+ `<port>`

### Bootstrapping your project
```bash
# Navigate to your workspace
g8 https://ajayv711@bitbucket.org/ajayv711/scala-sbt-vertx.g8.git 

# Check if everything if nice and dandy and not broken
sbt clean compile package

# Good, now lets add our first verticle

# Open sbt shell from project root directory
sbt
> g8Scaffold verticle
# Follow the configurations, and make sure to select yes for isWebVerticle to create an HTTP server
```
Voila! You have a new microservice running at `http://localhost:<port>` 
Feel the echo vibrations at `http://localhost:<port>/api/<version>/<name>`

### Developer tools
```bash
# Compiling
sbt clean compile

# Packaging
sbt clean package

# Building a fat jar
sbt clean assembly

# Running tests
sbt test

# SonarQube analysis
sbt -DsonarScanner.home=$SONAR_SCANNER_HOME sonarScan
```

### Issues
The terminal can sometimes act up and not accept an <ENTER> keypress. Run `stty sane` and rerun.