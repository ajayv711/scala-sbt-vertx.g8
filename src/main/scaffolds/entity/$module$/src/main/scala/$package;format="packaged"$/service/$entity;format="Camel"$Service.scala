package $package$.service

import java.lang.Long

import $package$.entity.$entity;format="Camel"$
import $package$.repository.$entity;format="Camel"$Repository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class $entity;format="Camel"$Service {

  @Autowired
  private var repo: $entity;format="Camel"$Repository = _

  def get$entity;format="Camel"$(): java.util.List[$entity;format="Camel"$] = {
    repo.findAll()
  }

  def save($entity;format="camel"$: $entity;format="Camel"$): $entity;format="Camel"$ = {
    repo.save($entity;format="camel"$)
  }

  def get$entity;format="Camel"$(id: Long): Option[$entity;format="Camel"$] = {
    val $entity;format="camel"$ = repo.findById(id)
    if ($entity;format="camel"$.isPresent) Some($entity;format="camel"$.get())
    else None
  }

}
