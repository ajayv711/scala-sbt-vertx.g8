package $package$.repository

import java.lang.Long

import $package$.entity.$entity;format="Camel"$
import org.springframework.data.jpa.repository.JpaRepository

trait $entity;format="Camel"$Repository extends JpaRepository[$entity;format="Camel"$, Long]