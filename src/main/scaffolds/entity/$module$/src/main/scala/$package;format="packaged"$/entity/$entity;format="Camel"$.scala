package $package$.entity

import java.lang.Long

import com.google.gson.annotations.SerializedName
import javax.persistence._

import scala.beans.BeanProperty

@Entity
@Table(name = "$entity;format="upper,snake"$")
class $entity;format="Camel"$ {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @SerializedName("id")
  @BeanProperty
  var id: Long = _

}