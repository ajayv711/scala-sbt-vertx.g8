package $package$.verticles

import $package;format="package"$.utils.VertxUtils._
import io.vertx.lang.scala.{ScalaVerticle, VertxExecutionContext}
import io.vertx.scala.core.Vertx
import io.vertx.scala.core.http.HttpServer
import io.vertx.scala.ext.web.Router
import io.vertx.scala.ext.web.handler.BodyHandler

import scala.concurrent.Future
import scala.util.{Failure, Success}

class $verticle;format="Camel"$Verticle extends ScalaVerticle {

  override def startFuture(): Future[_] = {
    $if(isWebVerticle.truthy)$
    val server: HttpServer = vertx.createHttpServer()
    val router: Router = Router.router(vertx)

    router.route().handler(BodyHandler.create)

    router.get("/").handler(r => r.response().setStatusCode(200).end("$project$ REST API"))
    router.get("/api/$version$/ping").handler(r => r.response().setStatusCode(200).end("pong"))

    server.requestHandler(router accept _)
      .listenFuture($port$)
    $endif$
  }

}

$if(addRunner.truthy)$
object $verticle;format="Camel"$Verticle extends App {

  implicit val vertx: Vertx = Vertx.vertx()
  implicit val vertxExecutionContext: VertxExecutionContext =
    VertxExecutionContext(vertx.getOrCreateContext)

  deployVerticle[$verticle;format="Camel"$Verticle]

  sys.addShutdownHook {
    vertx.close()
  }

}
$endif$