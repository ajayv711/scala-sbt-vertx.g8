package $organization;format="package"$.$package;format="package"$

import $organization;format="package"$.$package;format="package"$.utils.VertxUtils._
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx

object Application extends App {
  implicit val vertx: Vertx = Vertx.vertx()
  implicit val vertxExecutionContext: VertxExecutionContext =
    VertxExecutionContext(vertx.getOrCreateContext)

  // deploy verticles here
  // deployVerticle[$name;format="Camel"$Service]

  sys.addShutdownHook {
    vertx.close()
  }

}