package $organization;format="package"$.$package;format="package"$.utils

import java.io.File

import io.vertx.lang.scala.{ScalaVerticle, VertxExecutionContext}
import io.vertx.scala.core.Vertx

import scala.collection.mutable
import scala.reflect.runtime.universe._
import scala.util.{Failure, Success}

object VertxUtils {

  /**
    * Generously ripped off from https://dzone.com/articles/get-all-classes-within-package
    *
    * @param directory
    * @param packageName
    * @return
    */
  private def findClasses(directory: File, packageName: String): Seq[String] = {
    val classes = mutable.ArrayBuffer.empty[String]
    if (!directory.exists()) classes

    val files = directory.listFiles()
    for (file <- files) {
      if (file.isDirectory) {
        assert(!file.getName.contains("."))
        classes.appendAll(findClasses(file, packageName + "." + file.getName))
      } else if (file.getName.endsWith(".class") && !file.getName.contains("\$")) {
        classes.append(packageName + "." + file.getName.substring(0, file.getName.length - 6))
      }
    }
    classes
  }

  def getVerticles(packageName: String) = {
    val classLoader = getClass.getClassLoader
    val resources = classLoader.getResources(packageName.replace('.', '/'))
    val verticles = mutable.ArrayBuffer.empty[File]
    while (resources.hasMoreElements) {
      val resource = resources.nextElement()
      val file = new File(resource.getFile)
      verticles.append(file)
    }
    val classes = mutable.ArrayBuffer.empty[String]
    for (dir <- verticles) {
      classes.appendAll(findClasses(dir, packageName))
    }
    classes
  }

  def deployVerticleFuture[T <: ScalaVerticle : TypeTag](implicit vertx: Vertx, vertxExecutionContext: VertxExecutionContext): Unit = {
    vertx.deployVerticleFuture(ScalaVerticle.nameForVerticle[T]).onComplete {
      case Success(id) =>
        println(id)
      case Failure(e) =>
        e.printStackTrace()
        vertx.close()
    }
  }

  def deployVerticleFuture(name: String)(implicit vertx: Vertx, vertxExecutionContext: VertxExecutionContext): Unit = {
    vertx.deployVerticleFuture(s"scala:\$name").onComplete {
      case Success(id) =>
        println(id)
      case Failure(e) =>
        e.printStackTrace()
        vertx.close()
    }
  }

}
