## $name;format="norm"$
TODO

### What's in the Box??
+ `scoverage` configurations for code coverage
+ `scalastyle` configurations for code formatting 
+ `scapegoat` configurations for static code analysis
+ `sonarQube` configurations for continuous inspection
+ `vertx` boilerplate for an HTTP webserver
+ `circe` for json parsing
+ `scalatest` for unit testing
+ `scalacheck` for property-based testing
+ `logback` configurations for logging

### Prerequisites
+ Java 1.8+
+ SBT 1.0.0+
```bash
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt
```
+ SonarScanner 3.2.0+
+ `sonar-scala` plugin for SonarQube - [Copy JAR](https://dl.bintray.com/mwz/maven/com/github/mwz/sonar-scala_2.12/6.5.1/sonar-scala_2.12-6.5.1-assembly.jar) to `/opt/sonarqube/extensions/plugins`
+ SonarQube 6.7+ _Optional to install locally_

### How to run this project
Check if everything if nice and dandy and not broken
```
sbt clean compile package
```

Good, now lets add our first verticle. Open sbt shell from project root directory
```
sbt
> g8Scaffold verticle
```
Follow the configurations, and make sure to select yes for `isWebVerticle` to create an HTTP server

Voila! You have a new microservice running at `http://localhost:<port>` 

Play pingpong at `http://localhost:<port>/api/v$version$/ping`

### Developer tools
```bash
# Compiling
sbt clean compile

# Packaging
sbt clean package

# Building a fat jar
sbt clean assembly

# Running tests
sbt test

# SonarQube analysis
sbt -DsonarScanner.home=\$SONAR_SCANNER_HOME sonarScan
```