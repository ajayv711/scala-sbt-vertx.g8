lazy val build = (project in file("."))
  .settings(
    inThisBuild(
      List(
        organization := "$organization;format="package"$",
        scalaVersion := "2.12.4",
        version := "$version$",
        scapegoatVersion := "1.3.3"
      )
    ),
    name := "$name;format="norm"$",
    commonSettings,
    sonarUseExternalConfig := true,
    aggregate in sonarScan := false,
    aggregate in assembly := false,
    mainClass in (Compile, run) := Some("$organization;format="package"$.$package;format="package"$.Application"),
    mainClass in assembly := Some("$organization;format="package"$.$package;format="package"$.Application"),
    assemblyJarName in assembly := "$name;format="norm"$.jar",
    dockerfile in docker := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"app/\${artifact.name}"

      new Dockerfile {
        from("openjdk:8-jre")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )
  .enablePlugins(DockerPlugin)
  .dependsOn(core)
  .aggregate(commons, core)

lazy val commons = (project in file("$name;format="norm"$-commons"))
  .settings(
    name := "$name;format="norm"$-commons",
    commonSettings,
    libraryDependencies ++= testDependencies ++ circeDependencies ++ apacheDependencies
  )

lazy val core = (project in file("$name;format="norm"$-core"))
  .settings(
    name := "$name;format="norm"$-core",
    commonSettings,
    libraryDependencies ++= testDependencies ++ vertxDependencies
  )
  .dependsOn(commons)
  .aggregate(commons)

lazy val dependencies = new {
  private val scalatestVersion = "3.0.0"
  private val scalacheckVersion = "1.14.0"
  private val vertxVersion = "3.5.0"
  private val circeVersion = "0.9.3"
  private val circeYamlVersion = "0.6.1"
  private val commonsValidatorVersion = "1.6"
  private val commonsEmailVersion = "1.5"

  val scalatest = "org.scalatest" %% "scalatest" %  scalatestVersion
  val scalacheck = "org.scalacheck" %% "scalacheck" % scalacheckVersion

  val vertx = "io.vertx" %% "vertx-lang-scala" % vertxVersion
  val vertxWeb = "io.vertx" %% "vertx-web-scala" % vertxVersion
  val vertxAuthJwt = "io.vertx" %% "vertx-auth-jwt-scala" % vertxVersion

  val circeCore = "io.circe" %% "circe-core" % circeVersion
  val circeParser = "io.circe" %% "circe-parser" % circeVersion
  val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
  val circeGenericExtras = "io.circe" %% "circe-generic-extras" % circeVersion
  val circeYaml = "io.circe" %% "circe-yaml" % circeYamlVersion

  val commonsValidator = "commons-validator" % "commons-validator" % commonsValidatorVersion
  val commonsEmail = "org.apache.commons" % "commons-email" % commonsEmailVersion

}

lazy val testDependencies = Seq(
  dependencies.scalatest % Test,
  dependencies.scalacheck % Test
)

lazy val vertxDependencies = Seq(
  dependencies.vertx,
  dependencies.vertxWeb,
  dependencies.vertxAuthJwt
)

lazy val circeDependencies = Seq(
  dependencies.circeCore,
  dependencies.circeParser,
  dependencies.circeGeneric,
  dependencies.circeGenericExtras,
  dependencies.circeYaml
)

lazy val apacheDependencies = Seq(
  dependencies.commonsValidator,
  dependencies.commonsEmail
)

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(
    "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  ),
  fork in run := true,
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
    case PathList("META-INF", xs @ _*) => MergeStrategy.last
    case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
    case PathList("codegen.json") => MergeStrategy.discard
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  },
  coverageEnabled in sonarScan := true,
  coverageMinimum := 80,
  coverageFailOnMinimum := false
)